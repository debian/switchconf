Source: switchconf
Section: admin
Priority: optional
Maintainer: Jose M Calhariz <calhariz@debian.org>
Build-Depends: debhelper (>> 9.20160709),
Standards-Version: 4.4.0
Vcs-Browser: https://salsa.debian.org/debian/switchconf
Vcs-Git: https://salsa.debian.org/debian/switchconf.git
Homepage: http://blog.calhariz.com


Package: switchconf
Architecture: all
Depends: ${shlibs:Depends}, ${misc:Depends}, lsb-base (>= 3.0-6), liblockfile1,
Description: switch between system configuration sets
 Switchconf allows users to easily change their system's settings,
 choosing between the possible configurations for different
 environments. 
 .
 Most switchconf users are laptop owners who want to change their
 network settings according to where they currently are - but there
 are many cases for non mobile systems to desire to choose between
 configuration sets. 
 .
 Switchconf is a very simple script that takes its arguments from the
 command line. It allows you to maintain different sets of
 configurations, installing the desired set of configuration when
 called with its name as an argument, and execute a pre-specified set
 of commands before and after the switch.
