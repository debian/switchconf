#!/usr/bin/make -f
# -*- makefile -*-

PACKNAME=$(shell dh_listpackages)
SOURCENAME=$(shell dpkg-parsechangelog | grep "Source:" | cut -d ' ' -f 2)
DEBVER=$(shell dpkg-parsechangelog | grep "Version:" | cut -d ' ' -f 2)
VER=$(shell dpkg-parsechangelog | grep "Version:" | cut -d ' ' -f 2 | cut -f 1 -d -)

BINDIR=/usr/sbin
ETCDIR=/etc/$(PACKNAME)
MANDIR=/usr/share/man/man8
LOGROTATEDIR=/etc/logrotate.d
INITDIR=/etc/init.d
DISTFILE=$(SOURCENAME)_$(VER).orig.tar.xz
DISTFILES=$(DISTFILE) $(SOURCENAME)_$(DEBVER).debian.tar.gz


all:
	echo $(VER) $(DEBVER) $(MDIST) $(DIST) $(PACKNAME) $(SOURCENAME)
	echo $(DEB_HOST_GNU_TYPE) $(DEB_BUILD_GNU_TYPE)


configure:

build:

install:
	[ -d $(DESTDIR)/$(MANDIR) ] || mkdir -p $(DESTDIR)/$(MANDIR)
	cp $(PACKNAME).8 $(DESTDIR)/$(MANDIR)
	[ -d $(DESTDIR)/$(ETCDIR) ] || mkdir -p $(DESTDIR)/$(ETCDIR)
	[ -d $(DESTDIR)/$(ETCDIR)/before.d ] || mkdir -p $(DESTDIR)/$(ETCDIR)/before.d
	[ -d $(DESTDIR)/$(ETCDIR)/after.d ] || mkdir -p $(DESTDIR)/$(ETCDIR)/after.d
	[ -d $(DESTDIR)/$(LOGROTATEDIR)/ ] || mkdir -p $(DESTDIR)/$(LOGROTATEDIR)
	cp logrotate $(DESTDIR)/$(LOGROTATEDIR)/$(PACKNAME)
	cp conf $(DESTDIR)/$(ETCDIR)
	cp plugins.before/* $(DESTDIR)/$(ETCDIR)/before.d/
	cp plugins.after/* $(DESTDIR)/$(ETCDIR)/after.d/
	install -D init.d $(DESTDIR)/$(INITDIR)/$(PACKNAME)
	install -D $(PACKNAME) $(DESTDIR)/$(BINDIR)/$(PACKNAME)

test:
	./test1
	./test2

clean:
	find -name '*~' -exec rm {} \;
	#rm -f configure-up-stamp unpack-up-stamp build-up-stamp install-up-stamp

#Set on target rules the policy for tags
tag-release:
	git tag release/$(VER)

tag-debian:
	git tag debian/$(DEBVER)

# Experimental rule to build archive for public distribution
dist: clean
	if test -e ../$(DISTFILE) ; then echo "$(DISTFILE) file should be generated only once" ; false ; fi
	git checkout release/$(VER) && cd .. && tar --exclude='.svn' --exclude='debian' --exclude='tmp' --exclude='*~' --exclude='.git' -cf - $(SOURCENAME) | xz -v9 -c > $(DISTFILE)

deb:
	git checkout debian/$(DEBVER)
	debuild -uc -us
	fakeroot debian/rules clean

dev:
	if test -e ../$(DISTFILE) ; then echo "$(DISTFILE) file should be generated only once" ; false ; fi
	cd .. && tar --exclude='.svn' --exclude='debian' --exclude='tmp' --exclude='*~' --exclude='.git' -cf - $(SOURCENAME) | xz -v9 -c > $(DISTFILE)
	debuild -uc -us
	fakeroot debian/rules clean
	mv -f ../$(DISTFILE) ../$(DISTFILE).dev


.PHONY: build clean binary-indep binary-arch binary install configure
